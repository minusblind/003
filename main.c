/**
 *Este programa pide datos al usuario para realizar operaciones aritmeticas
 *entre otras aplicando los diferentes operadores existentes en el lenguaje c
 *
 *@author Alejandro Castro Rivera
 *@date 18 de marzo de 2021
 */

#include <stdio.h>

int main()
{
    /*Aritmeticos*/

    /*Se declaran las variables para almacenar los tres valores enteros que ocuparemos para este operador */
    int nume1, nume2, nume3;

    /*Se le pide al usuario el primer numero entero para sumar */
    printf("Escriba un dato entero para sumar: ");

    /*El programa guarda el primer numero entero para la suma */
    scanf(" %d", &nume1);

    /*Se le pide al usuario un segundo numero entero para sumar */
    printf("Escriba un segundo dato para sumar: ");

    /*El programa guarda el segundo numero entero para la suma */
    scanf(" %d", &nume2);

    /*El programa realiza la suma de los numeros enteros almacenados en las variables previamente declaradas
      y se guarda el numero entero resultante en la tercer variable declarada para luego ser presentada al se�or usuario */
    nume3=nume1+nume2;

    /*El programa le presenta al usuario la suma de numeros enteros completada
      demostrando el uso del operador y presentando el resultado */
    printf("El resultado de la suma con numeros enteros es: %d + %d = %d\n", nume1, nume2, nume3);

    /*Se declaran las variables para almacenar los tres valores enteros largos que ocuparemos para este operador */
    long nums1, numes2, numes3;

    /*Se le pide al usuario el primer numero entero largo para restar */
    printf("Escriba un dato entero largo para restar: ");

    /*El programa guarda el primer numero entero largo para la resta */
    scanf(" %ld",&nums1);

    /*Se le pide al usuario un segundo numero entero largo para restar */
    printf("Escriba un segundo numero entero largo para restar: ");

    /*El programa guarda el segundo numero entero largo para la resta */
    scanf(" %ld", &numes2);

    /*El programa realiza la resta de los numeros enteros largos almacenados en las variables previamente declaradas
      y se guarda el numero entero largo resultante en la tercer variable declarada para luego ser presentada al se�or usuario */
    numes3=nums1-numes2;

    /*El programa le presenta al usuario la resta de numeros enteros largos completada
      demostrando el uso del operador y presentando el resultado */
    printf("El resultado de la resta con numeros enteros largos es: %ld - %ld= %ld\n", nums1, numes2, numes3);

    /*Se declaran las variables para almacenar los tres valores enteros positivos que ocuparemos para este operador */
    unsigned long unnum1, unnum2, unnum3;

    /*Se le pide al usuario el primer numero entero positivo para multiplicar */
    printf("Escriba un dato entero positivo para multiplicar: ");

    /*El programa guarda el primer numero entero positivo para la multiplicacion */
    scanf(" %ld",&unnum1);

    /*Se le pide al usuario un segundo numero entero positivo para multiplicar */
    printf("Escriba un segundo numero entero positivo para multiplicar: ");

    /*El programa guarda el segundo numero entero positivo para la multiplicacion */
    scanf(" %ld", &unnum2);

    /*El programa realiza la multiplicacion de los numeros enteros positivos almacenados en las variables previamente declaradas
      y se guarda el numero entero positivo resultante en la tercer variable declarada para luego ser presentada al se�or usuario */
    unnum3=unnum1*unnum2;

    /*El programa le presenta al usuario la multiplicacion de numeros enteros positivos completada
      demostrando el uso del operador y presentando el resultado */
    printf("El resultado de la multiplicacion con numeros enteros positivos es: %d*%d= %d\n", unnum1, unnum2, unnum3);

    /*Se declaran las variables para almacenar los tres valores decimales largos positivos que ocuparemos para este operador */
    double unnums1, unnumes2, unnumes3;

    /*Se le pide al usuario el primer numero decimal para dividir */
    printf("Escriba un numero decimal para dividir: ");

    /*El programa guarda el primer numero decimal para dividir */
    scanf(" %lf",&unnums1);

    /*Se le pide al usuario un segundo numero decimal para dividir */
    printf("Escriba un segundo numero decimal para dividir: ");

    /*El programa guarda el segundo numero decimal para la division */
    scanf(" %lf", &unnumes2);

    /*El programa realiza la division de los numeros decimales almacenados en las variables previamente declaradas
      y se guarda el numero decimal resultante en la tercer variable declarada para luego ser presentada al se�or usuario */
    unnumes3=unnums1/unnumes2;

    /*El programa le presenta al usuario la division de numeros decimales completada
      demostrando el uso del operador y presentando el resultado */
    printf("El resultado de la division con numeros decimales es: %lf / %lf= %lf\n", unnums1, unnumes2, unnumes3);

    /*Se declaran las variables para almacenar los tres valores enteros que ocuparemos para este operador */
    int numd1, numd2, numd3;

    /*Se le pide al usuario el primer numero entero para convertir en porcentaje  */
    printf("Escriba un numero entero para convertir en porcentaje: ");

    /*El programa guarda el primer numero entero para convertir en porcentaje */
    scanf(" %d",&numd1);

    /*Se le pide al usuario un segundo numero entero para convertir en porcentaje */
    printf("Escriba un segundo numero entero para convertir en porcentaje: ");

    /*El programa guarda el segundo numero entero para la conversion a porcentaje */
    scanf(" %d", &numd2);

    /*El programa realiza la division de los numeros enteros almacenados en las variables previamente declaradas
      y se guarda el numero entero resultante en la tercer variable declarada para luego ser presentada al se�or usuario */
    numd3 = numd1 % numd2;

    /*El programa le presenta al usuario el porcentaje de numeros enteros completado
      demostrando el uso del operador y presentando el resultado */
    printf("El resultado del porcentaje con numeros enteros es: %d % %d= %d\n", numd1, numd2, numd3);

    /*Relacionales*/

    /*Se declaran las variables para almacenar los dos valores enteros positivos que ocuparemos para este operador */
    unsigned int a, b;

    /*Se le pide al usuario el primer numero entero positivo para comparar */
    printf("Escriba un numero entero positivo para comparar: ");

    /*El programa guarda el primer numero entero positivo para comparar */
    scanf(" %u", &a);

    /*Se le pide al usuario un segundo numero entero positivo para comparar */
    printf("Escriba un segundo numero entero positivo para comparar: ");

    /*El programa guarda el segundo numero entero positivo para comparar */
    scanf(" %u", &b);

    /*El programa realiza la comparacion de los numeros enteros positivo almacenados en las variables previamente declaradas
      y se presenta al se�or usuario un 1 si es verdadero y un 0 si es falso */
    printf("%u > %u es %u \n", a, b, a > b);

     /*Se declaran las variables para almacenar los dos valores enteros positivos que ocuparemos para este operador */
    float uno, dos;

    /*Se le pide al usuario el primer numero entero positivo para comparar */
    printf("Escriba un numero entero positivo para comparar: ");

    /*El programa guarda el primer numero entero positivo para comparar */
    scanf(" %f", &uno);

    /*Se le pide al usuario un segundo numero entero positivo para comparar */
    printf("Escriba un segundo numero entero positivo para comparar: ");

    /*El programa guarda el segundo numero entero positivo para comparar */
    scanf(" %f", &dos);

    /*El programa realiza la comparacion de los numeros enteros positivo almacenados en las variables previamente declaradas
      y se presenta al se�or usuario un 1 si es verdadero y un 0 si es falso */
    printf(" %f < %f es %d \n", uno, dos, uno < dos);

     /*Se declaran las variables para almacenar los dos valores decimales que ocuparemos para este operador */
    double tres, cuatro;

    /*Se le pide al usuario el primer numero decimal para comparar */
    printf("Escriba un numero decimal para comparar: ");

    /*El programa guarda el primer numero decimal para comparar */
    scanf(" %lf", &tres);

    /*Se le pide al usuario un segundo numero decimal para comparar */
    printf("Escriba un segundo numero decimal para comparar: ");

    /*El programa guarda el segundo numero decimal para comparar */
    scanf(" %lf", &cuatro);

    /*El programa realiza la comparacion de los numeros decimales almacenados en las variables previamente declaradas
      y se presenta al se�or usuario un 1 si es verdadero y un 0 si es falso */
    printf("%lf >= %lf es %lf \n", tres, cuatro, tres >= cuatro);

    /*Se declaran las variables para almacenar los dos valores largos positivos que ocuparemos para este operador */
    unsigned long cinco, seis;

    /*Se le pide al usuario el primer numero largo positivo para comparar */
    printf("Escriba un numero largo positivo para comparar: ");

    /*El programa guarda el primer numero largo positivo para comparar */
    scanf(" %ld", &cinco);

    /*Se le pide al usuario un segundo numero largo positivo para comparar */
    printf("Escriba un segundo numero largo positivo para comparar: ");

    /*El programa guarda el segundo numero largo positivo para comparar */
    scanf(" %ld", &seis);

    /*El programa realiza la comparacion de los numeros largos positivos almacenados en las variables previamente declaradas
      y se presenta al se�or usuario un 1 si es verdadero y un 0 si es falso */
    printf("%ld <= %ld es %ld \n", cinco, seis, cinco <= seis);

    /*Se declaran las variables para almacenar los dos valores largos positivos que ocuparemos para este operador */
    int siete, ocho;

    /*Se le pide al usuario el primer numero entero para comparar */
    printf("Escriba un numero entero para comparar: ");

    /*El programa guarda el primer numero entero para comparar */
    scanf(" %d", &siete);

    /*Se le pide al usuario un segundo numero entero para comparar */
    printf("Escriba un segundo numero entero para comparar: ");

    /*El programa guarda el segundo numero entero para comparar */
    scanf(" %d", &ocho);

    /*El programa realiza la comparacion de los numeros enteros almacenados en las variables previamente declaradas
      y se presenta al se�or usuario un 1 si es verdadero y un 0 si es falso */
    printf("%d == %d es %d \n", siete, ocho, siete == ocho);

    /*Se declaran las variables para almacenar los dos valores largos enteros que ocuparemos para este operador */
    long one, two;

    /*Se le pide al usuario el primer numero largo entero para comparar */
    printf("Escriba un numero largo entero para comparar: ");

    /*El programa guarda el primer numero largo entero para comparar */
    scanf(" %ld", &one);

    /*Se le pide al usuario un segundo numero largo entero para comparar */
    printf("Escriba un segundo numero largo entero para comparar: ");

    /*El programa guarda el segundo numero largo entero para comparar */
    scanf(" %ld", &two);

    /*El programa realiza la comparacion de los numeros largos enteros almacenados en las variables previamente declaradas
      y se presenta al se�or usuario un 1 si es verdadero y un 0 si es falso */
    printf("%ld != %ld es %ld \n", one, two, one != two);

    /*Se declara la variable para almacenar el valor corto entero que ocuparemos para este operador */
    short sh;

    /*Incremento y decremento*/

    /*Se le pide al usuario el primer numero entero corto para adicionar 1 */
    printf("Escriba un numero corto entero para adicionar 1: ");

    /*El programa guarda el primer numero corto entero para adicionar 1 */
    scanf(" %d", &sh);

    /*El programa realiza la adicion de el numero corto enteros almacenado en la variable previamente declarada
      mas uno y se presenta al se�or usuario el resultado */
    printf(" el valor ya adicionado es igual a %d \n", ++sh);

    /*Se declaran las variables para almacenar el valor corto entero positivo que ocuparemos para este operador */
    unsigned short sh1;

    /*Se le pide al usuario el primer numero entero corto positivo para sustraer 1 */
    printf("Escriba un numero corto entero positivo para sustraer 1: ");

    /*El programa guarda el primer numero corto entero positivo para sustraer 1 */
    scanf(" %u", &sh);

    /*El programa realiza la adicion de el numero corto entero positivo almacenado en la variable previamente declarada
      menos uno y se presenta al se�or usuario el resultado */
    printf(" el valor ya substraido es igual a %u \n", --sh);

    /*Asingacion*/

    /*Se declaran las variables para almacenar el numero decimal  y el numero a igualar que ocuparemos para este operador */
    double la, sol=20;

    /*Se le pide al usuario el primer numero decimal para igualar a 20 */
    printf("Escriba un numero para guardar en la variable la y luego igualar a 20: ");

    /*El programa guarda el numero decimal para igualar a 20 */
    scanf(" %lf", &la);

    /*El programa realiza la igualacion de el numero decimal almacenado en la variable la */
    la = sol;


    /*Se presenta al se�or usuario el resultado de la operacion */
    printf(" la ahora es = a %lf\n", la);

    /*Se declaran las variables para almacenar el numero decimal y el sumatorio que ocuparemos para este operador */
    double re, mi=60;

    /*Se le pide al usuario el primer numero decimal para asignar a 60 */
    printf("Escriba un numero para guardar en la variable re asignar +60: ");

    /*El programa guarda el numero decimal para reasignar 60 */
    scanf(" %lf", &re);

    /*El programa realiza la reasignacion de el numero decimal almacenado en la variable re */
    re += mi;

    /*Se presenta al se�or usuario el resultado de la asignacion */
    printf(" re ahora es = a %lf\n", re);

    /*Se declaran las variables para almacenar el numero decimal y el sustratorio que ocuparemos para este operador */
    float rio, lago=56;

    /*Se le pide al usuario el primer numero decimal para reasignar 56 */
    printf("Escriba un numero para guardar en la variable rio y asignar -56: ");

    /*El programa guarda el numero decimal para reasignar 56 */
    scanf(" %f", &rio);

    /*El programa realiza la reasignacion de el numero decimal almacenado en la variable rio */
    rio -= lago;

    /*Se presenta al se�or usuario el resultado de la asignacion */
    printf(" rio ahora es = a %f\n", rio);

    /*Se declaran las variables para almacenar el numero entero largo y el multiplo que ocuparemos para este operador */
    unsigned long bravo, alfa=9;

    /*Se le pide al usuario el primer numero entero largo para reasignar 9 */
    printf("Escriba un numero para guardar en la variable bravo y asignar *9: ");

    /*El programa guarda el numero entero largo para reasignar 9 */
    scanf(" %lu", &bravo);

    /*El programa realiza la reasignacion de el numero entero largo almacenado en la variable bravo */
    bravo *= alfa;

    /*Se presenta al se�or usuario el resultado de la asignacion */
    printf(" bravo ahora es = a %lu\n", bravo);

    /*Se declaran las variables para almacenar el numero entero positivo y el dividendo que ocuparemos para este operador */
    unsigned int padre, madre=69;

    /*Se le pide al usuario el primer numero entero positivo para reasignar 69 */
    printf("Escriba un numero para guardar en la variable padre y asignar /69: ");

    /*El programa guarda el numero entero positivo para reasignar 69 */
    scanf(" %u", &padre);

    /*El programa realiza la reasignacion de el numero entero positivo almacenado en la variable padre */
    padre /= madre;

    /*Se presenta al se�or usuario el resultado de la asignacion */
    printf(" padre ahora es = a %u\n", padre);

    /*Se declaran las variables para almacenar el numero entero y el dividendo que ocuparemos para este operador */
    int arbol, flor=34;

    /*Se le pide al usuario el primer numero entero para reasignar 34 */
    printf("Escriba un numero para guardar en la variable arbol y asignar %34: ");

    /*El programa guarda el numero entero para reasignar 34 */
    scanf(" %d", &arbol);

    /*El programa realiza la reasignacion de el numero entero almacenado en la variable arbol */
    arbol %= flor;

    /*Se presenta al se�or usuario el resultado de la asignacion */
    printf(" arbol ahora es = a %d\n", arbol);

    /*Logicos*/

    /*Se declaran las variables para almacenar los numeros entero largos positivos que ocuparemos para este operador */
    long morado, rojo, blanco;
    int azul;

    /*Se le pide al usuario el primer numero largo entero para comparar en una operacion logica */
    printf("Escriba un numero largo entero para comparar en una operacion logica AND: ");

    /*El programa guarda el primer numero largo entero para comparar */
    scanf(" %ld", &morado);

    /*Se le pide al usuario un segundo numero largo entero para comparar en una operacion logica*/
    printf("Escriba un segundo numero largo entero para comparar en una operacion logica AND: ");

    /*El programa guarda el segundo numero largo entero para comparar en una operacion logica */
    scanf(" %ld", &rojo);

    /*Se le pide al usuario un tercer numero largo entero para comparar en una operacion logica*/
    printf("Escriba un tercer numero largo entero para comparar en una operacion logica AND: ");

    /*El programa guarda el tercer numero largo entero para comparar en una operacion logica */
    scanf(" %ld", &blanco);

    /*El programa realiza la comparacion de los numeros largos enteros almacenados en las variables previamente declaradas */
    azul = (morado != rojo) && (blanco < morado);


    /*Se presenta al se�or usuario un 1 si ambas expreciones son verdaderas y un 0 si alguna es falsa */
    printf("(%ld != %ld) && (%ld < %ld) es %d\n", morado, rojo, blanco, morado, azul);

    unsigned long verde, amarillo, negro;
    int cafe;

    /*Se le pide al usuario el primer numero largo entero positivo para comparar en una operacion logica */
    printf("Escriba un numero largo entero positivo para comparar en una operacion logica OR: ");

    /*El programa guarda el primer numero largo entero positivo para comparar */
    scanf(" %lu", &verde);

    /*Se le pide al usuario un segundo numero largo entero positivo para comparar en una operacion logica*/
    printf("Escriba un segundo numero largo entero positivo para comparar en una operacion logica OR: ");

    /*El programa guarda el segundo numero largo entero positivo para comparar en una operacion logica */
    scanf(" %lu", &amarillo);

    /*Se le pide al usuario un tercer numero largo entero positivo para comparar en una operacion logica*/
    printf("Escriba un tercer numero largo entero positivo para comparar en una operacion logica OR: ");

    /*El programa guarda el tercer numero largo entero positivo para comparar en una operacion logica */
    scanf(" %lu", &negro);

    /*El programa realiza la comparacion de los numeros largos enteros positivos almacenados en las variables previamente declaradas */
    cafe = (verde == negro) || (amarillo >= negro);


    /*Se presenta al se�or usuario un 1 si alguna exprecion es verdadera y un 0 si las dos son falsas */
    printf("(%lu == %lu) || (%lu >= %lu) es %d\n", verde, negro, amarillo, negro, cafe);

    unsigned int sata, pcie;
    int ide;

    /*Se le pide al usuario el primer numero positivo entero para comparar en una operacion logica */
    printf("Escriba un numero entero positivo para comparar en una operacion logica NOT: ");

    /*El programa guarda el primer numero positivo entero para comparar */
    scanf(" %u", &pcie);

    /*Se le pide al usuario un segundo numero positivo entero para comparar en una operacion logica*/
    printf("Escriba un segundo numero positivo entero para comparar en una operacion logica NOT: ");

    /*El programa guarda el segundo numero positivo entero para comparar en una operacion logica */
    scanf(" %u", &sata);

    /*El programa realiza la comparacion de los numeros positivos enteros almacenados en las variables previamente declaradas */
    ide = !(sata == pcie);


    /*Se presenta al se�or usuario un 1 si la exprecion es falsa y un 0 si la expresion es verdadera */
    printf("!(%u == %u) es %d\n", sata, pcie, ide);

    /*Manejo de bits*/

    /*Se declaran las variables para almacenar los caracteres que ocuparemos para este operador */
    int agua, aceite, mantequilla;

    /*Se le pide al usuario el primer caracter*/
    printf("Escriba un caracter para hacer la operacion de bits & : ");

    /*El programa guarda el caracter para operar */
    scanf(" %d", &agua);

    /*Se le pide al usuario el segundo caracter*/
    printf("Escriba un segundo caracter para hacer la operacion de bits & : ");

    /*El programa guarda el segundo caracter para operar */
    scanf(" %d", &aceite);

    /*El programa realiza la operacion de bits con los caracteres almacenados en las variables previamente declaradas */
    mantequilla=agua & aceite;

    /*Se presenta al se�or usuario el resultado de la operacion */
    printf(" %d & %d = %d\n", agua, aceite, mantequilla);

    /*Se declaran las variables para almacenar los numeros positivos que ocuparemos para este operador */
    unsigned int azucar, sal, harina;

    /*Se le pide al usuario el primer numero positivo*/
    printf("Escriba un numero positivo para hacer la operacion de bits | : ");

    /*El programa guarda el numero positivo para operar */
    scanf(" %u", &azucar);

    /*Se le pide al usuario el segundo numero positivo*/
    printf("Escriba un segundo numero positivo para hacer la operacion de bits | : ");

    /*El programa guarda el segundo numero positivo para operar */
    scanf(" %u", &sal);

    /*El programa realiza la operacion de bits con los numeros positivos almacenados en las variables previamente declaradas */
    harina=azucar | sal;

    /*Se presenta al se�or usuario el resultado de la operacion */
    printf(" %u | %u = %u\n", azucar, sal, harina);

    /*Se declaran las variables para almacenar los numeros que ocuparemos para este operador */
    int rapido, lento;

    /*Se le pide al usuario un numero */
    printf("Escriba un numero entero para hacer la operacion de bits ~ : ");

    /*El programa guarda el numero para operar */
    scanf(" %d", &rapido);

    /*El programa realiza la operacion de bits con el numero almacenado en las variable previamente declarada */
    lento= ~rapido;

    /*Se presenta al se�or usuario el resultado de la operacion */
    printf(" ~%d = %d\n", rapido, lento);

    /*Se declaran las variables para almacenar los numeros largos positivos que ocuparemos para este operador */
    unsigned long cereal, yogurt;

    /*Se le pide al usuario un numero */
    printf("Escriba un numero positivo largo para hacer la operacion de bits << : ");

    /*El programa guarda el numero positivo largo para operar */
    scanf(" %lu", &cereal);

    /*El programa realiza la operacion de bits con los numeros almacenados en las variables previamente declaradas */
    yogurt= cereal << 1;

    /*Se presenta al se�or usuario el resultado de la operacion */
    printf(" ~%lu<<1 = %lu\n", cereal, yogurt);

    /*Se declaran las variables para almacenar los numeros positivos cortos que ocuparemos para este operador */
    unsigned short silla, banco;

    /*Se le pide al usuario un numero corto positivo */
    printf("Escriba un numero positivo corto para hacer la operacion de bits >> : ");

    /*El programa guarda el numero positivo corto para operar */
    scanf(" %u", &silla);

    /*El programa realiza la operacion de bits con los numeros almacenados en las variables previamente declaradas */
    banco= silla >> 1;

    /*Se presenta al se�or usuario el resultado de la operacion */
    printf(" ~%u<<1 = %\n", silla, banco);

    /* Operador de tama�o */

    /*Se declaran las variables para almacenar los numeros que ocuparemos para este operador */
    short sala;
    unsigned int cocina;
    float ba�o;
    double patio;

    /*Se le pide al usuario un numero corto  */
    printf("Escriba un numero corto para mostrar el tama�o que ocupa en bytes ");

    /*El programa guarda el numero corto para operar */
    scanf(" %d", &sala);

    /*Se le pide al usuario un numero positivo */
    printf("Escriba un numero positivo para mostrar el tama�o que ocupa en bytes ");

    /*El programa guarda el numero positivo corto para operar */
    scanf(" %u", &cocina);

    /*Se le pide al usuario un numero decimal */
    printf("Escriba un numero decimal para mostrar el tama�o que ocupa en bytes ");

    /*El programa guarda el numero decimal para operar */
    scanf(" %f", &ba�o);

    /*El programa realiza la operacion con los numeros almacenados en las variables previamente declaradas */
    sala, cocina = 5

    /*El programa se finaliza presentado un estado de finalizacion normal */
    return 0;
}
